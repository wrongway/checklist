CheckList is a simple little tool to display lists defined in a text files.

Checkmarks are provided to use lists as checklists.


Download windows binaries in the Tag list.



List Format:

Each non-empty line in the text file is a list item. Item hierarchy can be defined
via tab stops.

    Item0

    Item1

        SubItem1-0
    
        SubItem1-1
    
    Item2