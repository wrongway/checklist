﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckList.Utility
{
    class StringUtility
    {
        public static string RemoveEscapeChars(string text)
        {
            char[] escapeChars = new[] { '\n', '\r', '\t', '\a' };

            return new string(text.Where(c => !escapeChars.Contains(c)).ToArray());
        }
    }
}
