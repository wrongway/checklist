﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CheckList
{
    public partial class CheckListMain : Form
    {
        private float m_standardFontSize = 10.0f;

        private bool m_controlPressed = false;
        private bool m_plusPressed = false;
        private bool m_minusPressed = false;
        private bool m_zeroPressed = false;

        public CheckListMain()
        {
            InitializeComponent();

            m_standardFontSize = treeView.Font.Size;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.IO.Stream stream = null;

            openFileDialog.Filter = "Check List|*.ckl|Text file|*.txt";

            if(openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((stream = openFileDialog.OpenFile()) != null)
                {
                    System.IO.StreamReader streamReader = new System.IO.StreamReader(stream);

                    string text = streamReader.ReadToEnd();

                    treeView.Nodes.Clear();

                    ParseCheckList(text);
                }
            }
        }

        private void ParseCheckList(string text)
        {
            List<Parser.ChecklistEntry> entries = Parser.CklParser.ParseCklString(text);

            FillTreeView(entries);
        }

        private void FillTreeView(List<Parser.ChecklistEntry> entries)
        {
            List<TreeNode> nodes = GetTreeNodesFromChecklistEntries(entries);

            foreach(TreeNode node in nodes)
            {
                treeView.Nodes.Add(node);
            }

            treeView.ExpandAll();
        }

        private List<TreeNode> GetTreeNodesFromChecklistEntries(List<Parser.ChecklistEntry> entries)
        {
            List<TreeNode> nodes = new List<TreeNode>();

            foreach (Parser.ChecklistEntry entry in entries)
            {
                TreeNode node = new TreeNode();
                node.Text = entry.Text;

                if(entry.URL.Length > 0)
                {
                    node.ForeColor = Color.Blue;
                }

                List<TreeNode> subNodes = GetTreeNodesFromChecklistEntries(entry.SubEntries);

                foreach(TreeNode subNode in subNodes)
                {
                    node.Nodes.Add(subNode);
                }

                nodes.Add(node);
            }

            return nodes;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            treeView.ExpandAll();
            UncheckAllTreeNodes();
        }

        private void UncheckAllTreeNodes()
        {
            UncheckTreeNodes(treeView.Nodes);
        }

        private void UncheckTreeNodes(TreeNodeCollection treeNodes)
        {
            foreach(TreeNode node in treeNodes)
            {
                node.Checked = false;

                UncheckTreeNodes(node.Nodes);
            }
        }

        private void largerFontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseFontSize();
        }

        private void smallerFontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DecreaseFontSize();
        }

        private void resetFontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetFontSize();
        }

        private void CheckListMain_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.ControlKey)
            {
                m_controlPressed = true;
            }
            else if(e.KeyCode == Keys.Add)
            {
                m_plusPressed = true;
            }
            else if(e.KeyCode == Keys.Subtract)
            {
                m_minusPressed = true;
            }
            else if(e.KeyCode == Keys.NumPad0)
            {
                m_zeroPressed = true;
            }

            if(m_controlPressed)
            {
                if(m_plusPressed)
                {
                    IncreaseFontSize();
                }
                else if(m_minusPressed)
                {
                    DecreaseFontSize();
                }
                else if(m_zeroPressed)
                {
                    ResetFontSize();
                }
            }
        }

        private void CheckListMain_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
            {
                m_controlPressed = false;
            }
            else if (e.KeyCode == Keys.Add)
            {
                m_plusPressed = false;
            }
            else if (e.KeyCode == Keys.Subtract)
            {
                m_minusPressed = false;
            }
            else if (e.KeyCode == Keys.NumPad0)
            {
                m_zeroPressed = false;
            }
        }

        private void mouseWheel(object sender, MouseEventArgs e)
        {
            if(m_controlPressed)
            {
                if (e.Delta > 0)
                {
                    IncreaseFontSize();
                }
                else if (e.Delta < 0)
                {
                    DecreaseFontSize();
                }
            }
        }

        private void IncreaseFontSize()
        {
            Font font = treeView.Font;
            float fontSize = Math.Min(font.Size + 1.0f, 24.0f);
            font = new Font(font.Name, fontSize, font.Style, font.Unit);
            treeView.Font = font;
        }

        private void DecreaseFontSize()
        {
            Font font = treeView.Font;
            float fontSize = Math.Max(font.Size - 1.0f, 6.0f);
            font = new Font(font.Name, fontSize, font.Style, font.Unit);
            treeView.Font = font;
        }

        private void ResetFontSize()
        {
            Font font = treeView.Font;
            font = new Font(font.Name, m_standardFontSize, font.Style, font.Unit);
            treeView.Font = font;
        }

        private void treeView_KeyDown(object sender, KeyEventArgs e)
        {
            CheckListMain_KeyDown(sender, e);
        }

        private void treeView_KeyUp(object sender, KeyEventArgs e)
        {
            CheckListMain_KeyUp(sender, e);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.Show();
        }

        private void treeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string url = Parser.CklParser.FindURL(e.Node.Text);
            if(url.Length > 0)
            {
                ProcessStartInfo sInfo = new ProcessStartInfo(url);
                Process.Start(sInfo);
            }
        }
    }
}
