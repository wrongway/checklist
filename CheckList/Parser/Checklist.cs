﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckList.Parser
{
    public class ChecklistEntry
    {
        private int m_level = 0;
        private string m_text = "";
        private string m_url = "";

        private List<ChecklistEntry> m_subEntries = new List<ChecklistEntry>();

        public int Level
        {
            get { return m_level; }
            set { m_level = value; }
        }

        public string Text
        {
            get { return m_text; }
            set { m_text = value; }
        }

        public string URL
        {
            get { return m_url; }
            set { m_url = value; }
        }

        public List<ChecklistEntry> SubEntries
        {
            get { return m_subEntries; }
            set { m_subEntries = value; }
        }
    }
}
