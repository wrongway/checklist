﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CheckList.Parser
{
    class CklParser
    {
        public static List<ChecklistEntry> ParseCklString(string fileContent)
        {
            List<string> lines = SeparateLines(fileContent);

            List<ChecklistEntry> entries = ParseLines(lines);

            entries = BuildHierarchy(entries);

            return entries;
        }

        private static List<String> SeparateLines(string text)
        {
            return text.Split('\n').ToList();
        }

        private static List<ChecklistEntry> ParseLines(List<string> lines)
        {
            List<ChecklistEntry> result = new List<ChecklistEntry>();

            foreach(string line in lines)
            {
                ChecklistEntry entry = ParseLine(line);

                string unescapedLine = Utility.StringUtility.RemoveEscapeChars(line); // remove all escape chars to get the correct text length
                if(entry != null && unescapedLine.Length > 0)
                {
                    result.Add(entry);
                }
            }

            return result;
        }

        private static ChecklistEntry ParseLine(string line)
        {
            ChecklistEntry entry = new ChecklistEntry();

            string unescapedLine = Utility.StringUtility.RemoveEscapeChars(line);
            string url = FindURL(line);

            int tabCount = GetLineLevel(line); // line.Count(x => x == '\t');

            line = line.Remove(0, tabCount);

            entry.Level = tabCount;
            entry.Text = unescapedLine;
            entry.URL = url;

            return entry;
        }

        private static List<ChecklistEntry> BuildHierarchy(List<ChecklistEntry> entries)
        {
            List<ChecklistEntry> result = new List<ChecklistEntry>();

            Stack<ChecklistEntry> parents = new Stack<ChecklistEntry>();

            foreach(ChecklistEntry entry in entries)
            {
                ChecklistEntry parent = null;
                if(parents.Count > 0)
                {
                    parent = parents.Pop();
                }

                if(parent == null)
                {
                    result.Add(entry);
                    parents.Push(entry);
                }
                else if(parent.Level == entry.Level)
                {
                    if(parents.Count <= 0)
                    {
                        result.Add(entry);
                        parents.Push(entry);
                    }
                    else
                    {
                        parent = parents.Pop();
                        parent.SubEntries.Add(entry);
                        parents.Push(parent);
                    }
                }
                else if(parent.Level < entry.Level)
                {
                    if(parent.SubEntries.Count > 0)
                    {
                        ChecklistEntry subEntry = parent.SubEntries.Last();
                        if(entry.Level == subEntry.Level)
                        {
                            parent.SubEntries.Add(entry);
                            parents.Push(parent);
                        }
                        else if(entry.Level > subEntry.Level)
                        {
                            subEntry.SubEntries.Add(entry);
                            parents.Push(parent);
                            parents.Push(subEntry);
                        }
                    }
                    else
                    {
                        parent.SubEntries.Add(entry);
                        parents.Push(parent);
                    }
                }
                else
                {
                    bool found = false;

                    while(parents.Count > 0)
                    {
                        parent = parents.Pop();

                        if(parent.Level < entry.Level)
                        {
                            parent.SubEntries.Add(entry);
                            found = true;
                            parents.Push(parent);
                            break;
                        }
                    }

                    if(found == false)
                    {
                        result.Add(entry);
                        parents.Push(entry);
                    }
                }
            }

            return result;
        }

        private static int GetLineLevel(string line)
        {
            int i = 0;
            while(line.Length > 0 && line[i] == '\t')
            {
                i++;
            }

            return i;
        }

        public static string FindURL(string line)
        {
            // source: http://stackoverflow.com/questions/9125016/get-url-from-a-text
            Regex regexUrl = new Regex(@"(http|ftp|https)://([\w+?\.\w+])+([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)?", RegexOptions.IgnoreCase);

            Match match = regexUrl.Match(line);

            return match.Value;
        }
    }
}
